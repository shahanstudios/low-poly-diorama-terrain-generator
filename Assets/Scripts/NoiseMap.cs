﻿using UnityEngine;

public class NoiseMap {
	private int width;
	private int height;

	private Noise noise;
	
	private float[,] noiseValues;

	public int Width => width;
	public int Height => height;

	private NoiseMap(int width, int height) {
		this.width = width;
		this.height = height;
		this.noiseValues = new float[width, height];

		this.noise = new Noise();
	}

	public float Evaluate(int x, int y) {
		return noiseValues[x,y];
	}

	public static NoiseMap GenerateNoiseMap(int mapWidth, int mapHeight, float scale, Vector2 offset, NoiseSettings noiseSettings) {
		NoiseMap map = new NoiseMap(mapWidth, mapHeight);

		if (scale <= 0) {
			scale = 0.0001f;
		}

		float amplitude = 1f;
		float frequency = 1f;

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;

		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++) {
				float elevation = 0;
				float sampleX, sampleY;
				float pointEvaluation;

				float pointAmplitude = amplitude;
				float pointFrequency = frequency;

				for (int i = 0; i < noiseSettings.octaves; i++) {
					sampleX = (x - halfWidth + offset.x) / scale * pointFrequency;
					sampleY = (y - halfHeight + offset.y) / scale * pointFrequency;
					pointEvaluation = map.noise.Evaluate(new Vector3(sampleX, 0, sampleY));

					elevation += pointAmplitude * pointEvaluation;
					pointFrequency *= noiseSettings.lacunarity;
					pointAmplitude *= noiseSettings.gain;
				}

				map.noiseValues[x, y] = elevation;
			}
		}

		return map;
	}

	[System.Serializable]
	public class NoiseSettings {
		public int octaves = 4;
		public float lacunarity = 2.0f;
		[Range(0,1)]
		public float gain = 0.5f;

		public void OnValidate() {
			if (octaves < 0) {
				octaves = 0;
			}
			if (lacunarity < 1) {
				lacunarity = 1;
			}
			gain = Mathf.Clamp01(gain);
		}
	}
}
