﻿using UnityEngine;

[CreateAssetMenu(menuName = "Density Functions/Sphere")]
public class SphereDensityFunc : DensityFunc {
	public Vector3 sphereCenter;
	public float radius;

	protected override float Evaluate(Vector3Int point) {
		int x1 = (int)((point.x - sphereCenter.x) * (point.x - sphereCenter.x));
		int y1 = (int)((point.y - sphereCenter.y) * (point.y - sphereCenter.y));
		int z1 = (int)((point.z - sphereCenter.z) * (point.z - sphereCenter.z));

		// distance between the
		// centre and given point
		return (x1 + y1 + z1) - (radius * radius);
	}
}
