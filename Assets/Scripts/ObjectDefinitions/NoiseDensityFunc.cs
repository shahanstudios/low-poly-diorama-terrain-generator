﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Density Functions/Noise")]
public class NoiseDensityFunc : DensityFunc {
	private int width;
	private int depth;
	private int height;

	private int vWidth => width + 1;
	private int vDepth => depth + 1;
	private int vHeight => height + 1;

	private int worldWidth => vWidth * worldSize.x;
	private int worldDepth => vDepth * worldSize.z;
	private int worldHeight => vHeight * worldSize.y;

	public float scale;
	public float noiseWeight;
	public bool closeEdges;
	public bool closeBottom;

	public NoiseMap.NoiseSettings settings;
	public Vector2 globalOffset;

	private Vector3Int worldSize;
	private Vector3Int chunkOffset;
	private Vector3 amps;
	private Func<float, float, float, float> offsetFunc;

	private NoiseMap map;
	public override void Initialize(Vector3Int densitySize, Vector3Int worldSize, Vector3Int chunkOffset) {
		this.width = densitySize.x;
		this.depth = densitySize.y;
		this.height = densitySize.z;
		this.worldSize = worldSize;
		this.chunkOffset = chunkOffset * new Vector3Int(vWidth, vHeight, vDepth);

		Vector2 offset = globalOffset + new Vector2(chunkOffset.x * width, chunkOffset.z * depth);
		map = NoiseMap.GenerateNoiseMap(vWidth, vDepth, scale, offset, settings);

		float d = 0.5f;
		Func<float, float> ampFunc = (dim) => 1 / (((dim * dim) / 4f) - (d * dim) + (d * d));
		amps = new Vector3(ampFunc(worldWidth), ampFunc(worldHeight), ampFunc(worldDepth));

		offsetFunc = (x, a, w) => a * ((x - w * 0.5f) * (x - w * 0.5f)) - 1;
	}
	protected override float Evaluate(Vector3Int point) {
		float noise = map.Evaluate(point.x, point.z);
		float finalVal = point.y + noise * noiseWeight;

		if (closeEdges) {
			Vector3 p = new Vector3(
				Mathf.InverseLerp(0, width, point.x) * vWidth,
				Mathf.InverseLerp(0, height, point.y) * vHeight,
				Mathf.InverseLerp(0, depth, point.z) * vDepth);
			Vector3 offset = new Vector3(
					offsetFunc(p.x + chunkOffset.x, amps.x, worldWidth),
					offsetFunc(p.y + chunkOffset.y, amps.y, worldHeight),
					offsetFunc(p.z + chunkOffset.z, amps.z, worldDepth)
			);
			float edgeWeight;
			if (closeBottom) {
				edgeWeight = Mathf.Clamp01(Mathf.Sign(Mathf.Max(Mathf.Max(offset.x, offset.z), offset.y)));
			}
			else {
				edgeWeight = Mathf.Clamp01(Mathf.Sign(Mathf.Max(offset.x, offset.z)));
			}
			finalVal = finalVal * (1 - edgeWeight) + 100f * edgeWeight;
		}

		return finalVal;
	}

	private Vector3 Abs(Vector3 a) {
		return new Vector3(Mathf.Abs(a.x), Mathf.Abs(a.y), Mathf.Abs(a.z));
	}
}
