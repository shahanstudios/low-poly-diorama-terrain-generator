﻿using System;
using UnityEngine;

public abstract class DensityFunc : ScriptableObject {
	public virtual void Initialize(Vector3Int densitySize, Vector3Int worldSize, Vector3Int offset) {}
	protected abstract float Evaluate(Vector3Int point);

	public Func<Vector3Int, float> GetFunc() {
		return (point) => Evaluate(point);
	}
}
