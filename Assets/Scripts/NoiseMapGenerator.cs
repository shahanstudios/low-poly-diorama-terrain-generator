﻿using System;
using UnityEngine;

public class NoiseMapGenerator : MonoBehaviour {
	public int mapWidth;
	public int mapHeight;

	public float scale = 1f;

	public NoiseMap.NoiseSettings settings;

	public Vector2 offset;

	public bool autoUpdate;

	public event Action<NoiseMap> OnGenerateNoiseMap;

	public void GenerateMap() {
		NoiseMap noiseMap = NoiseMap.GenerateNoiseMap(mapWidth, mapHeight, scale, offset, settings);
		OnGenerateNoiseMap?.Invoke(noiseMap);
	}


	private void OnValidate() {
		if (mapWidth < 2) {
			mapWidth = 2;
		}
		if (mapHeight < 2) {
			mapHeight = 2;
		}

		settings.OnValidate();
	}
}
