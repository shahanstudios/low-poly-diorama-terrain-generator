﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class TerrainDisplay : MonoBehaviour {
	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;

	private void Awake() {
		meshFilter = GetComponent<MeshFilter>();
		meshRenderer = GetComponent<MeshRenderer>();

		if (TryGetComponent(out NoiseMapGenerator mapGenerator)) {
			mapGenerator.OnGenerateNoiseMap += DrawMesh;
		}
	}
	private void Reset() {
		SetDrawOnGenerate();
	}

	private void SetDrawOnGenerate() {
		if (TryGetComponent(out NoiseMapGenerator mapGenerator)) {
			mapGenerator.OnGenerateNoiseMap += DrawMesh;
		}
	}

	public void DrawMesh(NoiseMap noiseMap) {
		var meshData = MeshGenerator.GenerateTerrainMesh(noiseMap);
		meshFilter.sharedMesh = meshData.CreateMesh();
	}
}
