﻿using System;
using Point = UnityEngine.Vector3Int;

public class DensityVolume {
	private int width;
	private int depth;
	private int height;

	private Voxel[,,] voxels;

	public Voxel[,,] Voxels => voxels;

	public DensityVolume(int width, int depth, int height, DensityFunc densityFunction) {
		this.width = width;
		this.depth = depth;
		this.height = height;

		Func<Point, float> posToDensityFunc = densityFunction.GetFunc();

		voxels = new Voxel[width, height, depth];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				for (int z = 0; z < depth; z++) {
					voxels[x, y, z] = new Voxel(new Point(x, y, z), posToDensityFunc);
				}
			}
		}
	}

	public class Voxel {
		private Point[] cornerPositions;
		private float[] cornerDensities;

		public Voxel(Point pos, Func<Point, float> densityFunc) {
			cornerPositions = new Point[8];

			cornerPositions[0] = new Point(pos.x, pos.y, pos.z);
			cornerPositions[1] = new Point(pos.x, pos.y + 1, pos.z);
			cornerPositions[2] = new Point(pos.x + 1, pos.y + 1, pos.z);
			cornerPositions[3] = new Point(pos.x + 1, pos.y, pos.z);

			cornerPositions[4] = new Point(pos.x, pos.y, pos.z + 1);
			cornerPositions[5] = new Point(pos.x, pos.y + 1, pos.z + 1);
			cornerPositions[6] = new Point(pos.x + 1, pos.y + 1, pos.z + 1);
			cornerPositions[7] = new Point(pos.x + 1, pos.y, pos.z + 1);

			cornerDensities = new float[8];

			for (int i = 0; i < cornerDensities.Length; i++) {
				cornerDensities[i] = densityFunc.Invoke(cornerPositions[i]);
			}
		}

		public Point GetCornerPosition(int index) {
			return cornerPositions[index];
		}
		public float GetCornerDensity(int index) {
			return cornerDensities[index];
		}

		public byte GetLookupValue(float isoLevel) {
			byte v = 0;
			for (int i = 0; i < cornerDensities.Length; i++) {
				if (cornerDensities[i] < isoLevel) {
					v |= (byte)(1 << i);
				}
			}
			return v;
		}
	}
}
