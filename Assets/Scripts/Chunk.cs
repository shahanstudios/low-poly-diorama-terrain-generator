﻿using UnityEngine;
using Point = UnityEngine.Vector3Int;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Chunk : MonoBehaviour {
	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;

	private Point coord;

	public Point Coord => coord;

	private void Awake() {
		meshFilter = GetComponent<MeshFilter>();
		meshRenderer = GetComponent<MeshRenderer>();
	}
	public void Setup(Material material, Point coord) {
		meshRenderer.material = material;
		this.coord = coord;
	}

	public void SetMeshData(MarchingCubesMeshGenerator.MeshData meshData) {
		meshFilter.sharedMesh = meshData.CreateMesh();
	}
}
