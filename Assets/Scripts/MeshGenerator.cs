﻿using UnityEngine;

public static class MeshGenerator {
	public static MeshData GenerateTerrainMesh(NoiseMap noiseMap) {
		int mapWidth = noiseMap.Width;
		int mapHeight = noiseMap.Height;

		float topLeftX = (mapWidth - 1) / -2f;
		float topLeftZ = (mapHeight - 1) / 2f;

		MeshData meshData = new MeshData(mapWidth, mapHeight);

		int vertexIndex = 0;
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				meshData.SetVertex(vertexIndex, new Vector3(topLeftX + x, noiseMap.Evaluate(x,y), topLeftZ - y));

				if (x < mapWidth - 1 && y < mapHeight - 1) {
					meshData.AddTriangle(vertexIndex, vertexIndex + mapWidth + 1, vertexIndex + mapWidth);
					meshData.AddTriangle(vertexIndex + mapWidth + 1, vertexIndex, vertexIndex + 1);
				}

				vertexIndex++;
			}
		}

		return meshData;
	}

	public class MeshData {
		private Vector3[] vertices;
		private int[] triangles;

		private int triangleIndex;

		public MeshData(int meshWidth, int meshHeight) {
			vertices = new Vector3[meshWidth * meshHeight];
			triangles = new int[(meshWidth - 1) * (meshHeight - 1) * 6];

			triangleIndex = 0;
		}
		public void SetVertex(int vertexIndex, Vector3 vertexPos) {
			vertices[vertexIndex] = vertexPos;
		}

		public void AddTriangle(int a, int b, int c) {
			triangles[triangleIndex++] = a;
			triangles[triangleIndex++] = b;
			triangles[triangleIndex++] = c;
		}

		public Mesh CreateMesh() {
			Mesh mesh = new Mesh();
			mesh.vertices = vertices;
			mesh.triangles = triangles;

			mesh.RecalculateNormals();

			return mesh;
		}
	}
}
