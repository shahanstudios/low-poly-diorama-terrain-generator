﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Point = UnityEngine.Vector3Int;

[RequireComponent(typeof(MeshRenderer))]
public class MarchingCubesMeshGenerator : MonoBehaviour {
	public Point numChunks = Point.one;
	public Point volumeBoundsSize = Point.one * 10;
	public Point numberOfVoxelsPerAxis = Point.one;
	public float isoLevel = 0;
	public DensityFunc densityFunction;

	public bool interpolatePoints = true;
	private bool smoothVerts = false;

	private MeshRenderer meshRenderer;
	private List<Chunk> chunks;

	private void Awake() {
		meshRenderer = GetComponent<MeshRenderer>();

		CreateChunks();
	}

	private void Start() {
		GenerateDensityMesh();
	}
	private void OnValidate() {
	}

	private void CreateChunks() {
		chunks = new List<Chunk>(numChunks.x * numChunks.y * numChunks.z);
		for (int x = 0; x < numChunks.x; x++) {
			for (int y = 0; y < numChunks.y; y++) {
				for (int z = 0; z < numChunks.z; z++) {
					GameObject chunkObj = new GameObject();
					chunkObj.name = $"Chunk {x},{y},{z}";
					chunkObj.transform.parent = this.transform;
					Chunk chunk = chunkObj.AddComponent<Chunk>();
					Point coord = new Point(x, y, z);
					chunk.Setup(meshRenderer.material, coord);
					chunks.Add(chunk);
				}
			}
		}
	}

	public DensityVolume GenerateVolume(Chunk chunk) {
		DensityFunc densityFunc = GameObject.Instantiate(densityFunction);
		densityFunc.Initialize(numberOfVoxelsPerAxis, numChunks, chunk.Coord);
		return new DensityVolume(numberOfVoxelsPerAxis.x, numberOfVoxelsPerAxis.z, numberOfVoxelsPerAxis.y, densityFunc);
	}

	public void GenerateDensityMesh() {
		foreach (Chunk chunk in chunks) {
			GenerateChunkMesh(chunk);
		}
	}

	public void GenerateChunkMesh(Chunk chunk) {
		chunk.transform.position = chunk.Coord * volumeBoundsSize;
		Func<Vector3, float, Vector3, float, Vector3> interpolate = (a, av, b, bv) => {
			float t = interpolatePoints ? (isoLevel - av) / (bv - av) : 0.5f;
			return Vector3.Lerp(a, b, t);
		};

		List<Triangle> triangles = new List<Triangle>();
		DensityVolume volume = GenerateVolume(chunk);
		foreach (var v in volume.Voxels) {
			byte lookup = v.GetLookupValue(isoLevel);

			int triCount;
			for (triCount = 0; MCLookupTables.TriTable[lookup, triCount] != -1; triCount += 3) {
				int a0 = MCLookupTables.CornerIndexAFromEdgeIndex[MCLookupTables.TriTable[lookup, triCount]];
				int b0 = MCLookupTables.CornerIndexBFromEdgeIndex[MCLookupTables.TriTable[lookup, triCount]];

				int a1 = MCLookupTables.CornerIndexAFromEdgeIndex[MCLookupTables.TriTable[lookup, triCount + 1]];
				int b1 = MCLookupTables.CornerIndexBFromEdgeIndex[MCLookupTables.TriTable[lookup, triCount + 1]];

				int a2 = MCLookupTables.CornerIndexAFromEdgeIndex[MCLookupTables.TriTable[lookup, triCount + 2]];
				int b2 = MCLookupTables.CornerIndexBFromEdgeIndex[MCLookupTables.TriTable[lookup, triCount + 2]];

				Triangle tri = new Triangle();
				Func<int, Vector3> getPos = (index) => {
					var volPos = v.GetCornerPosition(index);

					Vector3 rat = new Vector3(
							Mathf.InverseLerp(0, numberOfVoxelsPerAxis.x, volPos.x),
							Mathf.InverseLerp(0, numberOfVoxelsPerAxis.y, volPos.y),
							Mathf.InverseLerp(0, numberOfVoxelsPerAxis.z, volPos.z));
					return new Vector3(
							volumeBoundsSize.x * rat.x,
							volumeBoundsSize.y * rat.y,
							volumeBoundsSize.z * rat.z);
				};

				tri.a = interpolate(getPos(a0), v.GetCornerDensity(a0), getPos(b0), v.GetCornerDensity(b0));
				tri.b = interpolate(getPos(a1), v.GetCornerDensity(a1), getPos(b1), v.GetCornerDensity(b1));
				tri.c = interpolate(getPos(a2), v.GetCornerDensity(a2), getPos(b2), v.GetCornerDensity(b2));
				triangles.Add(tri);
			}
		}

		chunk.SetMeshData(new MeshData(triangles, smoothVerts));
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.white;
		if (chunks == null) {
			return;
		}
		foreach (Chunk chunk in chunks) {
			Gizmos.DrawWireCube((chunk.Coord * volumeBoundsSize) + volumeBoundsSize / 2, volumeBoundsSize);
		}
	}

	public class MeshData {
		private Vector3[] vertices;
		private int[] triangles;

		public MeshData(List<Triangle> triangles, bool dedupVerts = true) {
			if (dedupVerts) {
				List<Vector3> vertexList = new List<Vector3>();
				List<int> triangleIndexList = new List<int>();
				this.triangles = new int[triangles.Count * 3];
				Vector3 v;
				Dictionary<Vector3, int> vertexIndexMap = new Dictionary<Vector3, int>();
				for (int i = 0; i < triangles.Count; i++) {
					for (int j = 0; j < 3; j++) {
						v = triangles[i][j];
						if (!vertexIndexMap.ContainsKey(v)) {
							vertexIndexMap.Add(v, vertexIndexMap.Count);
							vertexList.Add(v);
						}
						this.triangles[i * 3 + j] = vertexIndexMap[v];
					}
				}
				this.vertices = vertexList.ToArray();
			}
			else {
				this.vertices = new Vector3[triangles.Count * 3];
				this.triangles = new int[triangles.Count * 3];
				Vector3 v;
				for (int i = 0; i < triangles.Count; i++) {
					for (int j = 0; j < 3; j++) {
						v = triangles[i][j];
						this.triangles[i * 3 + j] = i * 3 + j;
						this.vertices[i * 3 + j] = v;
					}
				}
			}
		}

		public Mesh CreateMesh() {
			Mesh mesh = new Mesh();
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.RecalculateNormals();

			return mesh;
		}
	}

	public struct Triangle {
		public Vector3 a, b, c;

		public Vector3 this[int i] {
			get {
				switch (i) {
					case 0:
						return a;
					case 1:
						return b;
					default:
						return c;
				}
			}
		}
	}
}
